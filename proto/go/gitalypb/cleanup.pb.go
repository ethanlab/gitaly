// Code generated by protoc-gen-go. DO NOT EDIT.
// source: cleanup.proto

package gitalypb

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type ApplyBfgObjectMapRequest struct {
	Repository *Repository `protobuf:"bytes,1,opt,name=repository,proto3" json:"repository,omitempty"`
	// A raw object-map file as generated by BFG: https://rtyley.github.io/bfg-repo-cleaner
	// Each line in the file has two object SHAs, space-separated - the original
	// SHA of the object, and the SHA after BFG has rewritten the object.
	ObjectMap            []byte   `protobuf:"bytes,2,opt,name=object_map,json=objectMap,proto3" json:"object_map,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ApplyBfgObjectMapRequest) Reset()         { *m = ApplyBfgObjectMapRequest{} }
func (m *ApplyBfgObjectMapRequest) String() string { return proto.CompactTextString(m) }
func (*ApplyBfgObjectMapRequest) ProtoMessage()    {}
func (*ApplyBfgObjectMapRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_1b19e990e4662c9c, []int{0}
}

func (m *ApplyBfgObjectMapRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ApplyBfgObjectMapRequest.Unmarshal(m, b)
}
func (m *ApplyBfgObjectMapRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ApplyBfgObjectMapRequest.Marshal(b, m, deterministic)
}
func (m *ApplyBfgObjectMapRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ApplyBfgObjectMapRequest.Merge(m, src)
}
func (m *ApplyBfgObjectMapRequest) XXX_Size() int {
	return xxx_messageInfo_ApplyBfgObjectMapRequest.Size(m)
}
func (m *ApplyBfgObjectMapRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ApplyBfgObjectMapRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ApplyBfgObjectMapRequest proto.InternalMessageInfo

func (m *ApplyBfgObjectMapRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *ApplyBfgObjectMapRequest) GetObjectMap() []byte {
	if m != nil {
		return m.ObjectMap
	}
	return nil
}

type ApplyBfgObjectMapResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ApplyBfgObjectMapResponse) Reset()         { *m = ApplyBfgObjectMapResponse{} }
func (m *ApplyBfgObjectMapResponse) String() string { return proto.CompactTextString(m) }
func (*ApplyBfgObjectMapResponse) ProtoMessage()    {}
func (*ApplyBfgObjectMapResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_1b19e990e4662c9c, []int{1}
}

func (m *ApplyBfgObjectMapResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ApplyBfgObjectMapResponse.Unmarshal(m, b)
}
func (m *ApplyBfgObjectMapResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ApplyBfgObjectMapResponse.Marshal(b, m, deterministic)
}
func (m *ApplyBfgObjectMapResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ApplyBfgObjectMapResponse.Merge(m, src)
}
func (m *ApplyBfgObjectMapResponse) XXX_Size() int {
	return xxx_messageInfo_ApplyBfgObjectMapResponse.Size(m)
}
func (m *ApplyBfgObjectMapResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ApplyBfgObjectMapResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ApplyBfgObjectMapResponse proto.InternalMessageInfo

type ApplyBfgObjectMapStreamRequest struct {
	// Only available on the first message
	Repository *Repository `protobuf:"bytes,1,opt,name=repository,proto3" json:"repository,omitempty"`
	// A raw object-map file as generated by BFG: https://rtyley.github.io/bfg-repo-cleaner
	// Each line in the file has two object SHAs, space-separated - the original
	// SHA of the object, and the SHA after BFG has rewritten the object.
	ObjectMap            []byte   `protobuf:"bytes,2,opt,name=object_map,json=objectMap,proto3" json:"object_map,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ApplyBfgObjectMapStreamRequest) Reset()         { *m = ApplyBfgObjectMapStreamRequest{} }
func (m *ApplyBfgObjectMapStreamRequest) String() string { return proto.CompactTextString(m) }
func (*ApplyBfgObjectMapStreamRequest) ProtoMessage()    {}
func (*ApplyBfgObjectMapStreamRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_1b19e990e4662c9c, []int{2}
}

func (m *ApplyBfgObjectMapStreamRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ApplyBfgObjectMapStreamRequest.Unmarshal(m, b)
}
func (m *ApplyBfgObjectMapStreamRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ApplyBfgObjectMapStreamRequest.Marshal(b, m, deterministic)
}
func (m *ApplyBfgObjectMapStreamRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ApplyBfgObjectMapStreamRequest.Merge(m, src)
}
func (m *ApplyBfgObjectMapStreamRequest) XXX_Size() int {
	return xxx_messageInfo_ApplyBfgObjectMapStreamRequest.Size(m)
}
func (m *ApplyBfgObjectMapStreamRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ApplyBfgObjectMapStreamRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ApplyBfgObjectMapStreamRequest proto.InternalMessageInfo

func (m *ApplyBfgObjectMapStreamRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *ApplyBfgObjectMapStreamRequest) GetObjectMap() []byte {
	if m != nil {
		return m.ObjectMap
	}
	return nil
}

type ApplyBfgObjectMapStreamResponse struct {
	Entries              []*ApplyBfgObjectMapStreamResponse_Entry `protobuf:"bytes,1,rep,name=entries,proto3" json:"entries,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                                 `json:"-"`
	XXX_unrecognized     []byte                                   `json:"-"`
	XXX_sizecache        int32                                    `json:"-"`
}

func (m *ApplyBfgObjectMapStreamResponse) Reset()         { *m = ApplyBfgObjectMapStreamResponse{} }
func (m *ApplyBfgObjectMapStreamResponse) String() string { return proto.CompactTextString(m) }
func (*ApplyBfgObjectMapStreamResponse) ProtoMessage()    {}
func (*ApplyBfgObjectMapStreamResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_1b19e990e4662c9c, []int{3}
}

func (m *ApplyBfgObjectMapStreamResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ApplyBfgObjectMapStreamResponse.Unmarshal(m, b)
}
func (m *ApplyBfgObjectMapStreamResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ApplyBfgObjectMapStreamResponse.Marshal(b, m, deterministic)
}
func (m *ApplyBfgObjectMapStreamResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ApplyBfgObjectMapStreamResponse.Merge(m, src)
}
func (m *ApplyBfgObjectMapStreamResponse) XXX_Size() int {
	return xxx_messageInfo_ApplyBfgObjectMapStreamResponse.Size(m)
}
func (m *ApplyBfgObjectMapStreamResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ApplyBfgObjectMapStreamResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ApplyBfgObjectMapStreamResponse proto.InternalMessageInfo

func (m *ApplyBfgObjectMapStreamResponse) GetEntries() []*ApplyBfgObjectMapStreamResponse_Entry {
	if m != nil {
		return m.Entries
	}
	return nil
}

// We send back each parsed entry in the request's object map so the client
// can take action
type ApplyBfgObjectMapStreamResponse_Entry struct {
	Type                 ObjectType `protobuf:"varint,1,opt,name=type,proto3,enum=gitaly.ObjectType" json:"type,omitempty"`
	OldOid               string     `protobuf:"bytes,2,opt,name=old_oid,json=oldOid,proto3" json:"old_oid,omitempty"`
	NewOid               string     `protobuf:"bytes,3,opt,name=new_oid,json=newOid,proto3" json:"new_oid,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *ApplyBfgObjectMapStreamResponse_Entry) Reset()         { *m = ApplyBfgObjectMapStreamResponse_Entry{} }
func (m *ApplyBfgObjectMapStreamResponse_Entry) String() string { return proto.CompactTextString(m) }
func (*ApplyBfgObjectMapStreamResponse_Entry) ProtoMessage()    {}
func (*ApplyBfgObjectMapStreamResponse_Entry) Descriptor() ([]byte, []int) {
	return fileDescriptor_1b19e990e4662c9c, []int{3, 0}
}

func (m *ApplyBfgObjectMapStreamResponse_Entry) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ApplyBfgObjectMapStreamResponse_Entry.Unmarshal(m, b)
}
func (m *ApplyBfgObjectMapStreamResponse_Entry) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ApplyBfgObjectMapStreamResponse_Entry.Marshal(b, m, deterministic)
}
func (m *ApplyBfgObjectMapStreamResponse_Entry) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ApplyBfgObjectMapStreamResponse_Entry.Merge(m, src)
}
func (m *ApplyBfgObjectMapStreamResponse_Entry) XXX_Size() int {
	return xxx_messageInfo_ApplyBfgObjectMapStreamResponse_Entry.Size(m)
}
func (m *ApplyBfgObjectMapStreamResponse_Entry) XXX_DiscardUnknown() {
	xxx_messageInfo_ApplyBfgObjectMapStreamResponse_Entry.DiscardUnknown(m)
}

var xxx_messageInfo_ApplyBfgObjectMapStreamResponse_Entry proto.InternalMessageInfo

func (m *ApplyBfgObjectMapStreamResponse_Entry) GetType() ObjectType {
	if m != nil {
		return m.Type
	}
	return ObjectType_UNKNOWN
}

func (m *ApplyBfgObjectMapStreamResponse_Entry) GetOldOid() string {
	if m != nil {
		return m.OldOid
	}
	return ""
}

func (m *ApplyBfgObjectMapStreamResponse_Entry) GetNewOid() string {
	if m != nil {
		return m.NewOid
	}
	return ""
}

func init() {
	proto.RegisterType((*ApplyBfgObjectMapRequest)(nil), "gitaly.ApplyBfgObjectMapRequest")
	proto.RegisterType((*ApplyBfgObjectMapResponse)(nil), "gitaly.ApplyBfgObjectMapResponse")
	proto.RegisterType((*ApplyBfgObjectMapStreamRequest)(nil), "gitaly.ApplyBfgObjectMapStreamRequest")
	proto.RegisterType((*ApplyBfgObjectMapStreamResponse)(nil), "gitaly.ApplyBfgObjectMapStreamResponse")
	proto.RegisterType((*ApplyBfgObjectMapStreamResponse_Entry)(nil), "gitaly.ApplyBfgObjectMapStreamResponse.Entry")
}

func init() { proto.RegisterFile("cleanup.proto", fileDescriptor_1b19e990e4662c9c) }

var fileDescriptor_1b19e990e4662c9c = []byte{
	// 367 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xb4, 0x52, 0x4d, 0x6f, 0xda, 0x40,
	0x10, 0xd5, 0x96, 0x02, 0x65, 0xa0, 0x48, 0xdd, 0x0b, 0xae, 0xab, 0xb6, 0x2e, 0x07, 0xea, 0x0b,
	0x36, 0x75, 0x7f, 0x41, 0xa9, 0xaa, 0x9e, 0x22, 0x24, 0x93, 0x53, 0x2e, 0x68, 0x6d, 0x4f, 0x1c,
	0x47, 0xb6, 0x77, 0xb3, 0x5e, 0x82, 0xfc, 0x4b, 0xf2, 0xab, 0xf2, 0x67, 0x72, 0xcc, 0x29, 0x62,
	0x17, 0xf2, 0x21, 0x42, 0xc8, 0x25, 0x37, 0x7b, 0xde, 0xcc, 0x7b, 0x6f, 0xde, 0x2c, 0x7c, 0x8c,
	0x73, 0x64, 0xe5, 0x52, 0x78, 0x42, 0x72, 0xc5, 0x69, 0x2b, 0xcd, 0x14, 0xcb, 0x6b, 0xbb, 0x57,
	0x9d, 0x31, 0x89, 0x89, 0xa9, 0x0e, 0x0b, 0xb0, 0xfe, 0x08, 0x91, 0xd7, 0xd3, 0xd3, 0x74, 0x16,
	0x9d, 0x63, 0xac, 0x8e, 0x98, 0x08, 0xf1, 0x62, 0x89, 0x95, 0xa2, 0x01, 0x80, 0x44, 0xc1, 0xab,
	0x4c, 0x71, 0x59, 0x5b, 0xc4, 0x21, 0x6e, 0x37, 0xa0, 0x9e, 0xa1, 0xf1, 0xc2, 0x7b, 0x24, 0x7c,
	0xd4, 0x45, 0xbf, 0x02, 0x70, 0xcd, 0xb3, 0x28, 0x98, 0xb0, 0xde, 0x39, 0xc4, 0xed, 0x85, 0x1d,
	0xbe, 0x65, 0x1e, 0x7e, 0x81, 0xcf, 0xcf, 0xc8, 0x55, 0x82, 0x97, 0x15, 0x0e, 0x2b, 0xf8, 0xb6,
	0x03, 0xce, 0x95, 0x44, 0x56, 0xbc, 0xa1, 0xa3, 0x6b, 0x02, 0xdf, 0xf7, 0xaa, 0x1a, 0x63, 0xf4,
	0x3f, 0xb4, 0xb1, 0x54, 0x32, 0xc3, 0xca, 0x22, 0x4e, 0xc3, 0xed, 0x06, 0xe3, 0xad, 0xe6, 0x81,
	0x49, 0xef, 0x5f, 0xa9, 0x64, 0x1d, 0x6e, 0xa7, 0x6d, 0x06, 0x4d, 0x5d, 0xa1, 0x23, 0x78, 0xaf,
	0x6a, 0x81, 0x7a, 0x85, 0xfe, 0xc3, 0x0a, 0x86, 0xe6, 0xb8, 0x16, 0x18, 0x6a, 0x9c, 0x0e, 0xa0,
	0xcd, 0xf3, 0x64, 0xc1, 0xb3, 0x44, 0x3b, 0xef, 0x84, 0x2d, 0x9e, 0x27, 0xb3, 0x2c, 0x59, 0x03,
	0x25, 0xae, 0x34, 0xd0, 0x30, 0x40, 0x89, 0xab, 0x59, 0x96, 0x04, 0x37, 0x04, 0xfa, 0x7f, 0xcd,
	0xe1, 0xe7, 0x28, 0x2f, 0xb3, 0x18, 0x29, 0xc2, 0xa7, 0x1d, 0x9f, 0xd4, 0xd9, 0xbb, 0xc2, 0x26,
	0x6c, 0xfb, 0xc7, 0x0b, 0x1d, 0x9b, 0x8b, 0x75, 0x6e, 0xaf, 0xdc, 0xe6, 0x07, 0x62, 0x93, 0x5f,
	0x2e, 0xa1, 0x35, 0x0c, 0xf6, 0xc4, 0x41, 0x47, 0x07, 0xf3, 0x32, 0x92, 0x3f, 0x5f, 0x99, 0xeb,
	0x13, 0xe1, 0x09, 0x99, 0x4e, 0x4e, 0xd6, 0x83, 0x39, 0x8b, 0xbc, 0x98, 0x17, 0xbe, 0xf9, 0x1c,
	0x73, 0x99, 0xfa, 0x86, 0xce, 0xd7, 0x6f, 0xdd, 0x4f, 0xf9, 0xe6, 0x5f, 0x44, 0x51, 0x4b, 0x97,
	0x7e, 0xdf, 0x05, 0x00, 0x00, 0xff, 0xff, 0x5c, 0xdf, 0x1e, 0xf1, 0x25, 0x03, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// CleanupServiceClient is the client API for CleanupService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type CleanupServiceClient interface {
	// Deprecated in favour of ApplyBfgObjectMapStream
	ApplyBfgObjectMap(ctx context.Context, opts ...grpc.CallOption) (CleanupService_ApplyBfgObjectMapClient, error)
	ApplyBfgObjectMapStream(ctx context.Context, opts ...grpc.CallOption) (CleanupService_ApplyBfgObjectMapStreamClient, error)
}

type cleanupServiceClient struct {
	cc *grpc.ClientConn
}

func NewCleanupServiceClient(cc *grpc.ClientConn) CleanupServiceClient {
	return &cleanupServiceClient{cc}
}

func (c *cleanupServiceClient) ApplyBfgObjectMap(ctx context.Context, opts ...grpc.CallOption) (CleanupService_ApplyBfgObjectMapClient, error) {
	stream, err := c.cc.NewStream(ctx, &_CleanupService_serviceDesc.Streams[0], "/gitaly.CleanupService/ApplyBfgObjectMap", opts...)
	if err != nil {
		return nil, err
	}
	x := &cleanupServiceApplyBfgObjectMapClient{stream}
	return x, nil
}

type CleanupService_ApplyBfgObjectMapClient interface {
	Send(*ApplyBfgObjectMapRequest) error
	CloseAndRecv() (*ApplyBfgObjectMapResponse, error)
	grpc.ClientStream
}

type cleanupServiceApplyBfgObjectMapClient struct {
	grpc.ClientStream
}

func (x *cleanupServiceApplyBfgObjectMapClient) Send(m *ApplyBfgObjectMapRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *cleanupServiceApplyBfgObjectMapClient) CloseAndRecv() (*ApplyBfgObjectMapResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(ApplyBfgObjectMapResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *cleanupServiceClient) ApplyBfgObjectMapStream(ctx context.Context, opts ...grpc.CallOption) (CleanupService_ApplyBfgObjectMapStreamClient, error) {
	stream, err := c.cc.NewStream(ctx, &_CleanupService_serviceDesc.Streams[1], "/gitaly.CleanupService/ApplyBfgObjectMapStream", opts...)
	if err != nil {
		return nil, err
	}
	x := &cleanupServiceApplyBfgObjectMapStreamClient{stream}
	return x, nil
}

type CleanupService_ApplyBfgObjectMapStreamClient interface {
	Send(*ApplyBfgObjectMapStreamRequest) error
	Recv() (*ApplyBfgObjectMapStreamResponse, error)
	grpc.ClientStream
}

type cleanupServiceApplyBfgObjectMapStreamClient struct {
	grpc.ClientStream
}

func (x *cleanupServiceApplyBfgObjectMapStreamClient) Send(m *ApplyBfgObjectMapStreamRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *cleanupServiceApplyBfgObjectMapStreamClient) Recv() (*ApplyBfgObjectMapStreamResponse, error) {
	m := new(ApplyBfgObjectMapStreamResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// CleanupServiceServer is the server API for CleanupService service.
type CleanupServiceServer interface {
	// Deprecated in favour of ApplyBfgObjectMapStream
	ApplyBfgObjectMap(CleanupService_ApplyBfgObjectMapServer) error
	ApplyBfgObjectMapStream(CleanupService_ApplyBfgObjectMapStreamServer) error
}

// UnimplementedCleanupServiceServer can be embedded to have forward compatible implementations.
type UnimplementedCleanupServiceServer struct {
}

func (*UnimplementedCleanupServiceServer) ApplyBfgObjectMap(srv CleanupService_ApplyBfgObjectMapServer) error {
	return status.Errorf(codes.Unimplemented, "method ApplyBfgObjectMap not implemented")
}
func (*UnimplementedCleanupServiceServer) ApplyBfgObjectMapStream(srv CleanupService_ApplyBfgObjectMapStreamServer) error {
	return status.Errorf(codes.Unimplemented, "method ApplyBfgObjectMapStream not implemented")
}

func RegisterCleanupServiceServer(s *grpc.Server, srv CleanupServiceServer) {
	s.RegisterService(&_CleanupService_serviceDesc, srv)
}

func _CleanupService_ApplyBfgObjectMap_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(CleanupServiceServer).ApplyBfgObjectMap(&cleanupServiceApplyBfgObjectMapServer{stream})
}

type CleanupService_ApplyBfgObjectMapServer interface {
	SendAndClose(*ApplyBfgObjectMapResponse) error
	Recv() (*ApplyBfgObjectMapRequest, error)
	grpc.ServerStream
}

type cleanupServiceApplyBfgObjectMapServer struct {
	grpc.ServerStream
}

func (x *cleanupServiceApplyBfgObjectMapServer) SendAndClose(m *ApplyBfgObjectMapResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *cleanupServiceApplyBfgObjectMapServer) Recv() (*ApplyBfgObjectMapRequest, error) {
	m := new(ApplyBfgObjectMapRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _CleanupService_ApplyBfgObjectMapStream_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(CleanupServiceServer).ApplyBfgObjectMapStream(&cleanupServiceApplyBfgObjectMapStreamServer{stream})
}

type CleanupService_ApplyBfgObjectMapStreamServer interface {
	Send(*ApplyBfgObjectMapStreamResponse) error
	Recv() (*ApplyBfgObjectMapStreamRequest, error)
	grpc.ServerStream
}

type cleanupServiceApplyBfgObjectMapStreamServer struct {
	grpc.ServerStream
}

func (x *cleanupServiceApplyBfgObjectMapStreamServer) Send(m *ApplyBfgObjectMapStreamResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *cleanupServiceApplyBfgObjectMapStreamServer) Recv() (*ApplyBfgObjectMapStreamRequest, error) {
	m := new(ApplyBfgObjectMapStreamRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

var _CleanupService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "gitaly.CleanupService",
	HandlerType: (*CleanupServiceServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "ApplyBfgObjectMap",
			Handler:       _CleanupService_ApplyBfgObjectMap_Handler,
			ClientStreams: true,
		},
		{
			StreamName:    "ApplyBfgObjectMapStream",
			Handler:       _CleanupService_ApplyBfgObjectMapStream_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "cleanup.proto",
}
